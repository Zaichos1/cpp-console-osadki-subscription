#include "filter.h"
#include "osadki_subscription.h"
#include <cstring>
#include <fstream>
#include <iostream>

using namespace std;

osadki_subscription** filter(osadki_subscription* array[], int size, bool (*check)(osadki_subscription* element), int& result_size)
{
    osadki_subscription** result = new osadki_subscription * [size];
    result_size = 0;
    for (int i = 0; i < size; i++)
    {
        if (check(array[i]))
        {
            result[result_size++] = array[i];
        }
    }
    return result;
}
bool check_by_mess(osadki_subscription* element)
{
    return strcmp(element->characteristic, "�����") == 0;
}
bool check_by_time(osadki_subscription* element)
{
    return element->MainAmount.amount < 1.5;
}

void siftDown(osadki_subscription** report, int root, int bottom)
{
    int maxChild;
    int done = 0;

    while ((root * 2 <= bottom) && (!done))
    {
        if (root * 2 == bottom)
            maxChild = root * 2;
        else if (report[root * 2]->MainAmount.amount > report[root * 2 + 1]->MainAmount.amount)
            maxChild = root * 2;
        else
            maxChild = root * 2 + 1;

        if (report[root]->MainAmount.amount < report[maxChild]->MainAmount.amount)
        {

            std::swap(report[root], report[maxChild]);
            root = maxChild;
        }
        else
            done = 1;
    }
}

void InsertionSort(osadki_subscription** report, int size)
{

    for (int i = (size / 2); i >= 0; i--)
        siftDown(report, i, size - 1);
    for (int i = size - 1; i >= 1; i--)
    {
        std::swap(report[0], report[i]);
        siftDown(report, 0, i - 1);
    }
}
void Quick(osadki_subscription** report, int size)
{
    int mid = size / 2;
    if (size % 2 == 1)
        mid++;
    int h = 1;

    osadki_subscription** c = new osadki_subscription * [size];
    int step;
    while (h < size)
    {
        step = h;
        int i = 0;
        int j = mid;
        int k = 0;
        while (step <= mid)
        {
            while ((i < step) && (j < size) && (j < (mid + step)))
            {
                if (report[i]->MainAmount.amount > report[j]->MainAmount.amount)
                {
                    c[k] = report[i];
                    i++; k++;
                }
                else {
                    c[k] = report[j];
                    j++; k++;
                }
            }
            while (i < step)
            {
                c[k] = report[i];
                i++; k++;
            }
            while ((j < (mid + step)) && (j < size))
            {
                c[k] = report[j];
                j++; k++;
            }
            step = step + h;
        }
        h = h * 2;

        for (i = 0; i < size; i++)
            report[i] = c[i];
    }
}











void siftDown_str(osadki_subscription** report, int root, int bottom)
{
    int maxChild;
    int done = 0;

    while ((root * 2 <= bottom) && (!done))
    {
        if (root * 2 == bottom)
            maxChild = root * 2;


        else if (strlen(report[root * 2]->characteristic) > strlen(report[root * 2 + 1]->characteristic))
            maxChild = root * 2;
        else
            maxChild = root * 2 + 1;
        if (strlen(report[root]->characteristic) < strlen(report[maxChild]->characteristic))
        {

            std::swap(report[root], report[maxChild]);
            root = maxChild;
        }
        else
            done = 1;
    }
}

void InsertionSort_str(osadki_subscription** report, int size)
{

    for (int i = (size / 2); i >= 0; i--)
        siftDown_str(report, i, size - 1);
    for (int i = size - 1; i >= 1; i--)
    {
        std::swap(report[0], report[i]);
        siftDown_str(report, 0, i - 1);
    }
}
void Quick_str(osadki_subscription** report, int size)
{
    int mid = size / 2;
    if (size % 2 == 1)
        mid++;
    int h = 1;
    osadki_subscription** c = new osadki_subscription * [size];
    int step;
    while (h < size)
    {
        step = h;
        int i = 0;
        int j = mid;
        int k = 0;
        while (step <= mid)
        {
            while ((i < step) && (j < size) && (j < (mid + step)))
            {
                if (strlen(report[i]->characteristic) < strlen(report[j]->characteristic))
                {
                    c[k] = report[i];
                    i++; k++;
                }
                else {
                    c[k] = report[j];
                    j++; k++;
                }
            }
            while (i < step)
            {
                c[k] = report[i];
                i++; k++;
            }
            while ((j < (mid + step)) && (j < size))
            {
                c[k] = report[j];
                j++; k++;
            }
            step = step + h;
        }
        h = h * 2;

        for (i = 0; i < size; i++)
            report[i] = c[i];
    }
}
void mess_sort(osadki_subscription** report, int size)
{
    int i = 0;
    int buf;
    while (i < size)
    {
        int count = 0;
        char* author = report[i]->characteristic;
        for (int j = 0; j < size; j++)
        {
            if (strcmp(report[j]->characteristic, author) == 0)
                count++;
        }
        if (count == 1)
            i++;
        else
        {
            buf = i;
            osadki_subscription** c = new osadki_subscription * [count];
            for (int j = 0; j < count; j++)
            {
                c[j] = report[buf];
                buf++;

            }
            int min;
            for (int k = 0; k < count; k++)
            {
                min = k;
                for (int j = k + 1; j < count; j++)
                    if (c[j]->DayAndMonth.day < c[min]->DayAndMonth.day)
                        min = j;
                std::swap(c[k], c[min]);
            }
            for (int l = 0; l < count; l++)
            {
                report[i] = c[l];
                i++;
            }
        }
    }
}