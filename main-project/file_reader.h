#pragma once
#ifndef FILE_READER_H
#define FILE_READER_H

#include "osadki_subscription.h"

void read(const char* file_name, osadki_subscription* array[], int& size);

#endif