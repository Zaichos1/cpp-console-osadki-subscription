#pragma once
#ifndef OSADKI_SUBSCRIPTION_H
#define OSADKI_SUBSCRIPTION_H

#include "constants.h"

struct date
{
    int day;
    int month;
};

struct osadki
{
    double amount;
};

struct osadki_subscription
{
    
    date DayAndMonth;
    osadki MainAmount;
    char characteristic[MAX_STRING_SIZE];
};

#endif