#include <iostream>
#include "osadki_subscription.h"
#include "file_reader.h"
#include "constants.h"
#include "filter.h"

using namespace std;

void output(osadki_subscription* subscriptions)
{
    
    if (subscriptions->DayAndMonth.day < 61)
        cout << subscriptions->DayAndMonth.day << ' ';
    else
        cout << subscriptions->DayAndMonth.day << ' ';
    if (subscriptions->DayAndMonth.month < 61)
        cout << subscriptions->DayAndMonth.month << ' ';
    else
        cout << subscriptions->DayAndMonth.month << ' ';
        cout << subscriptions->MainAmount.amount << endl;
    cout << subscriptions->characteristic << '\n';
    cout << '\n';
}

int main()
{
    setlocale(LC_ALL, "RUS");
    cout << "Laboratory work #9. GIT\n";
    cout << "Variant #3. Osadki Subscription\n";
    cout << "Author: Dima Valutov\n";
    cout << "Group: 12\n";
    cout << endl;
    osadki_subscription* subscriptions[MAX_FILE_ROWS_COUNT];
    int size;
    try
    {
        read("data.txt", subscriptions, size);
        for (int i = 0; i < size; i++)
        {
            output(subscriptions[i]);
        }
        bool (*check_function)(osadki_subscription*) = NULL;
        cout << "\n�������� ������ ���������� ��� ��������� ������:\n";
        cout << "1) ����� ��� ���, � ������� ��� �����.\n";
        cout << "2) ����� ��� ���, � ������� ����� ������� ��� ������ 1,5.\n";
        cout << "\n������� ����� ���������� ������: ";
        int item;
        cin >> item;
        cout << '\n';
        switch (item)
        {
        case 1:
            check_function = check_by_mess; // ����������� � ��������� �� ������� ��������������� �������
            cout << "***** ����� ��� ���, � ������� ��� ����� *****\n\n";
            break;
        case 2:
            check_function = check_by_time; // ����������� � ��������� �� ������� ��������������� �������
            cout << "***** ����� ��� ���, � ������� ����� ������� ��� ������ 1,5. *****\n\n";
            break;
        default:
            throw "������������ ����� ������";
        }
        if (check_function)
        {
            int new_size;
            osadki_subscription** filtered = filter(subscriptions, size, check_function, new_size);
            cout << "�������� ��������:\n";
            cout << "1)������������� ���������� �� ����������� ���������� �������\n";
            cout << "2)���������� ��������� �� ����������� ���������� �������\n";
            cout << "3)������������� ���������� �� ����������� ��������������, � � ������ ����� �������������� �� ����������� ������ ������, � � ������ ������ ������ �� ����������� ������ ���\n";
            cout << "4)���������� ��������� �� ����������� ��������������, � � ������ ����� �������������� �� ����������� ������ ������, � � ������ ������ ������ �� ����������� ������ ���\n";
            cout << "\n������� ����� ���������� ������: ";
            int sort;
            cin >> sort;
            switch (sort)
            {
            case 1:
                InsertionSort(filtered, new_size);
                break;
            case 2:
                Quick(filtered, new_size);
                break;
            case 3:
                InsertionSort_str(filtered, new_size);
                Quick_str(filtered, new_size);
                break;
            case 4:
                Quick_str(filtered, new_size);
                mess_sort(filtered, new_size);
                break;
            default:
                throw "������������ ����� ������";
            }
            for (int i = 0; i < new_size; i++)
            {
                output(filtered[i]);
            }
            delete[] filtered;
        }
        for (int i = 0; i < size; i++)
        {
            delete subscriptions[i];
        }
    }
    catch (const char* error)
    {
        cout << error << '\n';
    }
    return 0;
}
