#ifndef FILTER_H
#define FILTER_H
#include "osadki_subscription.h"
osadki_subscription** filter(osadki_subscription* array[], int size, bool (*check)(osadki_subscription* element), int& result_size);
bool check_by_mess(osadki_subscription* element);
bool check_by_time(osadki_subscription* element);
void siftDown(osadki_subscription** report, int root, int bottom);
void InsertionSort(osadki_subscription** report, int size);
void Quick(osadki_subscription** report, int size);
void siftDown_str(osadki_subscription** report, int root, int bottom);
void InsertionSort_str(osadki_subscription** report, int size);
void Quick_str(osadki_subscription** report, int size);
void mess_sort(osadki_subscription** report, int size);
#endif